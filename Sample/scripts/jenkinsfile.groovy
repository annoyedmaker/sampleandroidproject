pipeline {
    agent any
    stages {
        stage("checkout") {
            steps {
                checkout([$class                           : 'GitSCM',
                          branches                         : [[name: 'master']],
                          doGenerateSubmoduleConfigurations: false,
                          extensions                       : [[$class: 'PruneStaleBranch']],
                          submoduleCfg                     : [],
                          userRemoteConfigs                : [[credentialsId: 'gitaccesscredentials',
                                                               url          : 'https://Ajinkya_Kolkhede@bitbucket.org/annoyedmaker/sampleandroidproject.git']]])
            }
        }
        stage("build") {
            steps {
                dir("Sample") {
                    sh './gradlew build clean'
                    echo "The build stage passed..."

                }
            }
        }
        stage("test") {
            steps {
                echo "The test stage passed..."
            }
        }
    }
    post {
        always {
            echo "post-build will always run after build completed"
            emailext body: "Build ${env.BUILD_NUMBER} running in job ${env.JOB_NAME} completed with status ${currentBuild.currentResult} \n More info at: ${env.BUILD_URL}", recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']], subject: "Jenkins Build job ${env.BUILD_NUMBER} completed with status ${currentBuild.currentResult}"
            cleanWs()
        }
    }
}