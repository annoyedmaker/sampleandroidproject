pipeline {
    agent any
    stages {
        stage("Danger") {
            steps {
                sh 'chmod +x Sample/dangerinstall'
                dir("Sample") {
                    echo "${env.BITBUCKET_PULL_REQUEST_ID}"
                    sh "./dangerinstall ${env.BITBUCKET_PULL_REQUEST_ID} ${env.BITBUCKET_PULL_REQUEST_LINK}"
                }
            }
        }
    }
    post {
        always {
            echo "post-build will always run after build completed"
        }
    }
}